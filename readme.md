# Desafio da semana 1 do PS 2020.2 IN Junior

# Tarefa de HTML e CSS para o fim de semana

Vocês irão desenvolver uma página para o projeto Brafé utilizando todos os ensinamentos dados nas aulas de HTML e CSS. Estou compartilhando o link de uma pasta com os conteúdos para o site e também vídeos mostrando alguns requisitos.
[link da pasta](https://drive.google.com/drive/folders/1ww3kgQOdAbnfZuOG5H44eJUVivGXDNch?usp=sharing)

Os requisitos para a página serão:
1. Animação nos links do menu de navegação do header
2. Hover nos botões
3. Fazer header fixo ao "scrollar" a página
4. Aplicar animação nas bolinhas da página utilizando keyframe
5. Ao clicar nos links do menu de navegação o usuário é diretamente direcionado para a parte da página em que o link está relacionado (baixa prioridade em relação aos outros requisitos)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

***O prazo de entrega do projeto será às 23:59 do domingo (02/08).***

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


### Referencias:

1. Flexbox navbar
https://www.alura.com.br/conteudo/posicione-elementos-com-flexbox

2. Ancorando elementos no html5
   https://www.alura.com.br/artigos/ancorando-elementos-com-html5



### TODO

* HTML:
1. Consertar tags de HTML semantico
2. Consertar navbar 
3. Add imagens corretas
4. .....

* CSS
1. font-family: Georgia, serif 
2. CSS flexbox
3. Animação nos links do menu de navegação do header
4. Hover nos botões ( usar pseudoclasse)
5. Fazer header fixo ao "scrollar" a página
6. Aplicar animação nas bolinhas da página utilizando keyframe
7. ......
